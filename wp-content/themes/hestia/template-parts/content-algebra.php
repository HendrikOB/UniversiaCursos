<?php
/**
 * The default template for displaying content on page builders templates.
 *
 * Used for page builder full width and page builder blank.
 *
 * @package Hestia
 * @since Hestia 1.1.24
 * @author Themeisle
 */ ?>
<article id="post-<?php the_ID(); ?>" class="section section-text pagebuilder-section">
<section id="courses">
<div class="container">
<div class="row">
<div class="col-sm-12">

	<?php the_content(); ?>

</div>
</div>
</div>
</section>

<div class="container-fluid">
<div class="row centered">
<div class="col-sm-3">
<ul class="list-group">

<?php wp_list_pages( array( 
'title_li' => '',
'child_of' => $id,
 ) ); ?>

</ul>

</div>
</div>
</div>
</section>


<section id="paginat">
<div class="container">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">
<?php
$pagelist = get_pages('sort_column=menu_order&sort_order=asc');
$pages = array();
foreach ($pagelist as $page) {
   $pages[] += $page->ID;
}

$current = array_search(get_the_ID(), $pages);
$prevID = $pages[$current-1];
$nextID = $pages[$current+1];
?>

<div class="navigation">
<?php if (!empty($prevID)) { ?>
<div class="alignleft">
<a href="<?php echo get_permalink($prevID); ?>"
  title="<?php echo get_the_title($prevID); ?>"><span aria-hidden="true">&larr;</span> Previous</a>
</div>

<?php }
if (!empty($nextID)) { ?>
<div class="alignright">
<a href="<?php echo get_permalink($nextID); ?>" 
 title="<?php echo get_the_title($nextID); ?>">Next <span aria-hidden="true">&rarr;</span></a>
</div>
<?php } ?>
</div><!-- .navigation -->

</div>
</div>
</div>
</section>



<section id="comment">
<div class="container">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">
<div id="disqus_thread"></div>
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT 
     *  THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR 
     *  PLATFORM OR CMS.
     *  
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: 
     *  https://disqus.com/admin/universalcode/#configuration-variables
     */

    var disqus_config = function () {
        // Replace PAGE_URL with your page's canonical URL variable
        this.page.url = '<?php echo get_page_link(); ?>';  
        
        // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        this.page.identifier = '<?php echo get_the_ID(); ?>'; 
    };
    
    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
        var d = document, s = d.createElement('script');
        
        // IMPORTANT: Replace EXAMPLE with your forum shortname!
        s.src = 'https://universiacursos-com.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>
    Please enable JavaScript to view the 
    <a href="https://disqus.com/?ref_noscript" rel="nofollow">
        comments powered by Disqus.
    </a>
</noscript>
</div>
</div>
</div>
</section>

</article>

